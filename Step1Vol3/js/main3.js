$(document).ready(function () {


    $(".our-services-nav li").click(function () {
        $(this).addClass("active").siblings().removeClass("active").closest(".our-services-content").find(".our-services-tabs-content").removeClass("active").eq($(this).index()).addClass("active");
    });
//Our amazing work tabs gallery
    const amazingImg = $(".our-amazing-work-img");
    const btnShowMore = $(".button-show-more");
    console.log(amazingImg.length);
    amazingImg.hide().slice(0, 12).show(200);
    btnShowMore.on("click", function () {
        $(".our-amazing-work-img:hidden").slice(0, 12).show(200);
        if ($(".our-amazing-work-img:hidden").length > 0) {
            btnShowMore.addClass("active")
        } else btnShowMore.removeClass("active")
    });
    $(".our-amazing-work-nav li").on("click", function () {
        amazingImg.hide();
        $(this).addClass("active").siblings().removeClass("active").closest(".our-amazing-work-content").find(`.our-amazing-work-img${$(this).data("target")}`).hide().slice(0,12).show(200);
        let thisHidden = $(`.our-amazing-work-img${$(this).data("target")}:hidden`);
        console.log(thisHidden);
        if (thisHidden.length <= 0) {
            btnShowMore.removeClass("active")
        }
        else btnShowMore.addClass("active");
    });
});
$(".break-news-content-card").click(function () {
                $(this).find(".break-news-content-card-right-top").css("background-color", "#18cfab");
                 $(this).find(".break-news-content-card-bottom-content-top-text").css("color", "#18cfab")
})
// FlipIt works uncorrect
// $(".our-amazing-work-images div img").justFlipIt({
//     Click: true,
//     // Style: [{el: 'self', style: {'margin': '0 auto', 'cursor': 'pointer'}}],
//     Template:
//         '                <div class="ui-card" style="margin: 0 auto; background: #f8fcfe;' +
//         '                                                  width: 100%;' +
//         '                                                    border-top: 2px #18cfab solid;' +
//         '                                                  height: 100%;">' +
//         '                </div>'
// });

$('.what-they-say-top').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.what-they-say-bottom'
});
$('.what-they-say-bottom').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.what-they-say-top',
    dots: false,
    arrows: true,
    centerMode: false,
    focusOnSelect: true
});
$(document).ready(function(){
    $('.masonry').masonry({
// указываем элемент-контейнер в котором расположены блоки для динамической верстки
        itemSelector: '.item',
// указываем класс элемента являющегося блоком в нашей сетке
        singleMode: false,
// true - если у вас все блоки одинаковой ширины
        isResizable: true,
// перестраивает блоки при изменении размеров окна
        isAnimated: true,
// анимируем перестроение блоков
        animationOptions: {
            queue: true,
            duration: 500
        },
        columnWidth: 400

        // ,
// опции анимации - очередь и продолжительность анимации
    });
});



