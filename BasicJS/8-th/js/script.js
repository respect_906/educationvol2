
 document.addEventListener("DOMContentLoaded", function () {
        console.log("DOM is ready");
        document.body.innerHTML = "<input id='input' placeholder='Price' value='' style='border: gray 2px solid'>";
        const input = document.getElementById('input');
            input.addEventListener("focus", function () {
                this.value = "";
                this.style.border = ('green 2px solid');
                this.style.outline = ('none');
                if (this.nextElementSibling)
                {this.nextElementSibling.remove()}
            });
            input.addEventListener("blur", function () {
                this.style.border = ('gray  2px solid');
                let inputValue = this.value;
                if ((inputValue >= 0)&&(inputValue))
                {let inputValueMessage = "";
                    inputValueMessage = document.createElement('span');
                const closeBtn = document.createElement('button');
                closeBtn.innerText = "X";
                    closeBtn.style.width = ("20px");
                    closeBtn.style.height = ("20px");
                    input.style.color = ("green");
                inputValueMessage.style.display = ('block');
                inputValueMessage.innerText = `Current price: $${inputValue}`;
                document.body.prepend(inputValueMessage);
                inputValueMessage.append(closeBtn);
                const thisInput = this;
                closeBtn.addEventListener("click", function () {
                        inputValueMessage.remove();
                    thisInput.value = "";
                });
                }
                 else if (inputValue < 0)
                    {let inputValueMessage = document.createElement('span');
                    inputValueMessage.style.display = ('block');
                    input.style.border = ('red 2px solid');
                    input.style.outline = ('none');
                    inputValueMessage.innerText = `Please enter correct price.`;
                    document.body.append(inputValueMessage)}

            })
    }
);
