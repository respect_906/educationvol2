const tabsLinks = Array.from(document.querySelectorAll(".tabs-title"));
const tabsContent = Array.from(document.querySelectorAll(".tabs-content"));
tabsLinks.forEach(item => {
        item.addEventListener("click", function () {
            tabsLinks.forEach(i => {
            i.classList.remove("active")
        });
        item.classList.add("active");
            const index = tabsLinks.indexOf(this);
            tabsContent.forEach( i => {
            i.classList.remove("active")
        });
            tabsContent[index].classList.add("active")
})
});
